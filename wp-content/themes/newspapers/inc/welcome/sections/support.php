<?php
/**
 * Support
 */
?>

<div id="support" class="newspapers-tab-pane">

	<h1><?php esc_html_e( 'Need more details?', 'newspapers' ); ?></h1>

	<p><?php printf( esc_html__( 'Please check our full documentation for detailed information on how to use %s.','newspapers'), 'newspapers' ); ?></p>

	<p>
		<a href="<?php echo esc_url( 'http://themezwp.com/newspapers-demo/documentation-usage/' ); ?>" class="button button-primary"><?php printf( esc_html__( '%s documentation', 'newspapers' ), 'newspapers' ); ?></a>
	</p>

</div>

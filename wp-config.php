<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webapp-ctj');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z2d .KZW1EOt,wtX}#.NX-$ Pe8^n^ }1o/Cq;rd_n_4;QOguf&;un(6_pNYPb[s');
define('SECURE_AUTH_KEY',  'ow(G@W+yU9]U4q5}u0L9C2AeU5?g*<x~sx^ho7tTFsFZ{Ut{;,_:^Ts4m_z ?L6x');
define('LOGGED_IN_KEY',    'uIH;98sTkXuc!(@33|F>E]IGIUpA<MeqVk0*n%*<M]tj=+-`y75@-xchx3?:By*}');
define('NONCE_KEY',        '5@4Rr*#B?BaL)|@1Ag:,gyS?H|wZ`G,~R;6 BG`O,]E46A~9.$>($a#H)7$~E!aD');
define('AUTH_SALT',        '< DW@0 =$+`>`;u~h<%%XB1A*=n1M9YoC?@J6Yl4v$o2JI;@bhR#Pb91ylut6%HZ');
define('SECURE_AUTH_SALT', 'mn]kGYSowx.JZN,nfCnf_q_VcYcum4iGx}2`?~qI:[oB1M?Ex`wflbvePEU8kSvG');
define('LOGGED_IN_SALT',   'Zq?X^2mt?dSoQ;q)1K8fKepOALI(hE?/av]{$tEbEMn)EW0ZJA[|07nekc50L|g9');
define('NONCE_SALT',       '|vAEu;*/6[ZP.{SEEsc[=_S/jZ3}3[m&YJDv`~p(zK*^TU$SMbiQXZ#CI^c:xpP5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
